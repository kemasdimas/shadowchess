// ShadowChess -- bootstrap.js
var app = this;

var app = this;
app.isConnected = false;

// Declare global style
app.mystyle = { };
app.mystyle.listFocus = function() {
    return {
        'background-color': '#eecf9a',
        color: '#000'
    };
};
app.mystyle.listBlur = function(bgColor) {
    return {
        'background-color': bgColor,
        color: '#fff'
    };
};

app.on('connected', function() {    
    app.isConnected = true;
    
    console.log('Application connected to backend.');
});
app.on('disconnected', function() {    
    app.isConnected = false;
    
    console.log('Application disconnected from backend.');
});