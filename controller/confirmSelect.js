var ui = require('ui'),
    _ = require('common/util'),
    Board = require('./lib/board').Board,
    Particles = require('./lib/particles').Particles,
    ImageView = ui.ImageView,
    TextView = ui.TextView,
    ListView = require('../lib/ListView').ListView;
    
var app = this;
    
_.extend(exports, {
    init: function(rawList, title) {
        var self = this;
        
        var list = [ ];
        var reuse;
        rawList.forEach(function(item) {
            reuse = new TextView({
                label: item.name,
                style: {
                    width: 'fill-parent',
                    border: '4 10'
                }
            });
            
            reuse.on('focus', function() {
                this.style(app.mystyle.listFocus());
            });
            reuse.on('blur', function() {
                this.style(app.mystyle.listBlur('#8e6744'));
            });
            
            reuse.callback = self.backToGame;
            if (item.callback !== undefined) {
                reuse.callback = item.callback;
            }
            
            list.push(reuse);
        });
        
        var listView = new ListView(list, title);
        self.add('list', listView);
    },
    
    ':load': function() {
        var self = this;
        
        app.on('message', function(action, data) {
            if (action === 'playerWin' || action === 'playerLose' || action === 'timeoutKick') {
                try {
                    self.backToGame();    
                } catch (error) { }
            }
        });
    },
    
    ':state': function(data) {
        this.parent = data.parent;
        this.init(data.list, data.title);
    },
    
    ':keypress': function(key) {
        
        // Delegate up, down, fire, b, B, t, T to ListView
        // b, B used to jump to the bottom of the list
        // t, T used to jump to the top of the list
        if (!this.isSelected && (key === 'up' || key === 'down'
               || key === '84' || key === '116'
               || key === '66' || key === '98' 
               || key === 'fire')) {
                   
           this.get('list').emit('keypress', key);   
        }
    },
    
    ':inactive': function() {
        this.clear();  
        delete this.parent;
    },
    
    backToGame: function() {        
        app.back();
    }
});