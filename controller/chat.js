var ui = require('ui'),
    _ = require('common/util'),
    ImageView = ui.ImageView,
    TextView = ui.TextView,
    ListView = require('../lib/ListView').ListView;
    
var app = this;
app.chess.storage = app.storage('chess');
    
var emptyChatText = 'Press fire or R to write message';
    
_.extend(exports, {

    ':load': function() {
        var self = this;
        
        var conversationPanel = self.get('conversationPanel');
        var fieldPanel = self.get('fieldPanel');
        var chatBox = fieldPanel.get('chatText');
        chatBox.value(emptyChatText);
        
        var listView = new ListView([]);
        conversationPanel.add('list', listView);
        
        chatBox.on('focus', function() {
            self.isChatFocused = true;
        });
        chatBox.on('blur', function() {            
            self.isChatFocused = false;
            
            self.tempText = chatBox.value();
            chatBox.value(emptyChatText);
        });
        chatBox.on('submit', function() {
            var userText = self.tempText;
            var timeStamp = app.serverTime();
            var side = app.chess.playerSide;
            
            if (userText.length > 0) {
                chatBox.value('Sending ...');
                
                // console.log('SUBMIT: ' + userText);
                // self.appendMessage(app.chess.playerSide, userText);
                app.msg('handleChatMessage', { side: side, text: userText });
                self.tempText = '';
            }
        });
        
        app.on('message', function(action, data) {
            if (action === 'chatAcknowledge') {
                chatBox.value('');
                chatBox.emit('keypress', 'fire');
                var i = 0;
            } else if (action === 'chatMessage') {
                var sender = data.sender;
                var text = data.text;
                
                self.appendMessage(sender, text);
            } else if (action === 'messageSynched') {
                var messages = data.messages;
                app.chess.resetChat = false;
                
                var chatList = self.get('conversationPanel').get('list');
                chatList.clearAll();
                
                messages.forEach(function(message) {
                    self.appendMessage(message.sender, message.text);
                });
            } else if (self.isActive && (action === 'playerWin' || action === 'playerLose' || action === 'timeoutKick')) {
                console.log('CHAT HOME!!!');
                try {
                    app.back();    
                } catch (error) { }
            }
        });
    },
    
    ':state': function() {
        var self = this;
        var title = self.get('title');
        
        title.label(app.chess.players[0] + ' VS ' + app.chess.players[1]);
    },
    
    ':active': function() {
        var self = this;
        self.isActive = true;
        
        // If the list size is === 0, better request chatmessages from the server
        if (app.chess.resetChat) {
            app.msg('handleSyncMessage', { });
        }
    },
    
    ':inactive': function() {
        this.isActive = false;  
    },
    
    appendMessage: function(from, body) {
        var self = this;
        var chatList = self.get('conversationPanel').get('list');
        var label = app.chess.players[from] + ':' + body;
        
        var sideColor = (from === 0 ? '#ab7c52' : '#8e6744');
        
        var reuse = new TextView({
            label: label,
            style: {
                width: 'fill-parent',
                border: '2 5',
                color: '#fff'
            }
        });
        reuse.on('focus', function() {
            this.style(app.mystyle.listFocus());
        });
        reuse.on('blur', function() {
            this.style(app.mystyle.listBlur(sideColor));
        });
        
        chatList.addItem(reuse);
        chatList.scrollBottom();
    },
    
    ':resized': function(width, height) {
        var self = this;
        console.log('resized ' + width + 'x' + height);
        
        var title = self.get('title');
        var conversationPanel = self.get('conversationPanel');
        var chatBoxPanel = self.get('fieldPanel');
        
        self.conversationPanelHeight = height;
        title.on('resized', function(w, h) {
            self.conversationPanelHeight -= h;
        });
        chatBoxPanel.on('resized', function(w, h) {
            self.conversationPanelHeight -= h;
        });
        conversationPanel.on('resized', function(w, h) {
            console.log('SUPPOSED H: ' + self.conversationPanelHeight);
            
            conversationPanel.style({
                height: self.conversationPanelHeight
            });
        });
    },
        
    ':keypress': function(key) {
        var self = this;
        var fieldPanel = self.get('fieldPanel');
        var chatBox = fieldPanel.get('chatText');
        var chatList = self.get('conversationPanel').get('list');
        
        if (key === '99' || key === '67') {
            chatList.clearAll();
        }
                
        if (!self.isChatFocused && (key === '114' || key === '82' || key === 'fire')) {
            chatBox.value('');
            chatBox.emit('keypress', 'fire');
        } else if (!self.isChatFocused && (key === 'up' || key === 'down')) {
            console.log('up - down');
            
            chatList.emit('keypress', key);
        } else {
            chatBox.emit('keypress', key);
        }
    },
    
    trim: function(str) {
    }
});