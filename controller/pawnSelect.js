var ui = require('ui'),
    _ = require('common/util'),
    Board = require('./lib/board').Board,
    Particles = require('./lib/particles').Particles,
    ImageView = ui.ImageView,
    TextView = ui.TextView,
    ListView = require('../lib/ListView').ListView;
    
var app = this;
    
_.extend(exports, {
    init: function() {
        var self = this;
        var nodeType = app.chess.board.nodeType;
        var types = [{
            name: 'Rook',
            type: nodeType.rook
        }, {
            name: 'Bishop',
            type: nodeType.bishop
        }, {
            name: 'Knight',
            type: nodeType.knight
        }, {
            name: 'Queen',
            type: nodeType.queen
        }];
        
        var list = [ ];
        var reuse;
        var counter = 0;
        types.forEach(function(item) {
            var stripeColor = (counter++ % 2 !== 0 ? '#ab7c52' : '#8e6744');
            
            reuse = new TextView({
                label: item.name,
                style: {
                    width: 'fill-parent',
                    border: '4 10'
                }
            });
            reuse.type = item.type;
            
            reuse.on('focus', function() {
                this.style(app.mystyle.listFocus());
            });
            reuse.on('blur', function() {
                this.style(app.mystyle.listBlur(stripeColor));
            });
            
            reuse.callback = function() {
                self.replacePawn(self.boardIndex, item.type);
            };
            
            list.push(reuse);
        });
        
        var listView = new ListView(list, 'Promote Pawn to?');
        self.add('list', listView);
        
        app.vibrate(100);
    },
    
    ':load': function() {
        var self = this;
        
        app.on('message', function(action, data) {
            if (action === 'replacePawn' || action === 'playerWin' || action === 'playerLose' || action === 'timeoutKick') {
                try {
                    self.backToGame();    
                } catch (error) { }
            }
        });
        
        self.on('back', function() {
           console.log('Do nothing, back only when promotion selected'); 
        });
    },
    
    ':state': function(data) {
        this.parent = data.parent;
        this.boardIndex = data.index;
        this.isSelected = false;
        this.init();
        
        // Test replace
        // this.replacePawn(data.index, 3);
    },
    
    ':keypress': function(key) {
        
        // Delegate up, down, fire, b, B, t, T to ListView
        // b, B used to jump to the bottom of the list
        // t, T used to jump to the top of the list
        if (!this.isSelected && (key === 'up' || key === 'down'
               || key === '84' || key === '116'
               || key === '66' || key === '98' 
               || key === 'fire')) {
                   
           this.get('list').emit('keypress', key);   
        }
    },
    
    ':inactive': function() {
        this.clear();  
        delete this.parent;
    },
    
    replacePawn: function(index, replacementType) {
        this.isSelected = true;
        
        if (this.parent.isMultiplayer()) {
            // Send replacepawn message to backend
            // params: { selectedIndex: selectedIndex, replacementType: replacementType }
            
            console.log('Send MSG replacepawn');
            app.msg('handleReplacePawn', { selectedIndex: index, replacementType: replacementType });
        } else {
            var node = app.chess.board.map[index];
            node.type = replacementType;
            
            this.backToGame();
        }
    },
    
    backToGame: function() {
        this.parent.updateGems();
        this.parent.switchTurn();    
        
        app.back();
    }
});