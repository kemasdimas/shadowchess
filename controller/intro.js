var ui = require('ui'),
    _ = require('common/util'),
    ImageView = ui.ImageView,
    TextView = ui.TextView,
    SceneView = ui.SceneView,
    ListView = require('../lib/ListView').ListView;
    
var app = this;
app.chess = { };
app.chess.gameTypes = {
    training: 0,
    multiplayer: 1  
};
app.chess.storage = app.storage('chess');

introLayers = {
    background: 0,
    logo: 1,
    menu: 2 
}; 

_.extend(exports, {

    ':load': function() {
        var self = this;
        
        self.add('scene', new SceneView({
            style: {
                width: 'fill-parent',
                height: 'fill-parent'
            }
        }));
        var scene = self.get('scene');
        scene.setLayers(3);
        
        scene.defineSpritesheet('bgtile', app.resourceURL('bgtile2.png'), 53, 54);
        scene.defineSpritesheet('logo', app.resourceURL('biglogo.png'), 220, 92);
        
        // Set background
        scene.setLayerBackground(introLayers.background, {
            sprite: 'bgtile',
            x: 0,
            y: 0,
            tile: true
        });
        
        // Set logo
        self.logo = scene.add({
            sprite: 'logo',
            x: 0,
            y: 0,
            layer: introLayers.logo,
            frame: 0
        });
        
        // self.get('image').src(app.resourceURL('biglogo.png'));
        
        var menus = [{
            text: 'Play Multiplayer',
            image: 'menu_multi.png',
            callback: function() {
                app.isBackFromGame = false;
                self.startMultiplayer();
            }
        }, {
            text: 'How to Play',
            image: 'menu_howto.png',
            callback: function() {
                app.setContent('howto', { });
            }
        }];
        
        var list = [ ];
        var reuse;
        menus.forEach(function(item) {
            reuse = new SceneView({
                style: {
                    width: 154,
                    height: 30
                }
            });
            reuse.setLayers(1);
            reuse.defineSpritesheet('background', app.resourceURL(item.image), 153, 30);
            
            var bg = reuse.add({
                sprite: 'background',
                x: 0,
                y: 0,
                layer: 0,
                frame: 0
            });
            
            reuse.on('focus', function() {
                reuse.change(bg, {
                    frame: 1
                });
            });
            reuse.on('blur', function() {
                reuse.change(bg, {
                    frame: 0
                });
            });
            
            reuse.callback = item.callback;
            
            list.push(reuse);
        });
        
        self.listView = new ListView(list, null);
        
        // Add the menu to scne
        scene.setLayerControl(introLayers.menu, self.listView);
        
        app.on('message', function(action, data) {
            if (action === 'noRoom') {
                try {
                    app.chess.storage.set('roomId', { });
                    self.startMultiplayer();
                } catch (error) { }
            }
        });
    },
    
    ':resized': function(width, height) {
        var self = this;
        var scene = self.get('scene');
        var isPortrait = false;
        var transY = 10;
        
        if (width < height) {
            isProtrait = true;
            transY = 40;
        }
        
        scene.changeLayer(introLayers.background, {
            width: width, 
            height: height
        });
        
        scene.translate(introLayers.logo, (width - 220) / 2, transY);
        scene.translate(introLayers.menu, (width - 153) / 2, transY + 100);
    },
    
    startMultiplayer: function() {
        console.log('ROOM ID: ' + app.chess.storage.get('roomId'));
        var roomId = app.chess.storage.get('roomId');
        if (roomId.roomId) {
            app.msg('joinRoom', { resync: true });
        } else {
            app.setContent('roomSelect', { });
        }
    },
    
    ':keypress': function(key) {
        
        // Delegate up, down, fire, b, B, t, T to ListView
        // b, B used to jump to the bottom of the list
        // t, T used to jump to the top of the list
        if (!this.isSelected && (key === 'up' || key === 'down'
               || key === '84' || key === '116'
               || key === '66' || key === '98' 
               || key === 'fire')) {
                   
           this.listView.emit('keypress', key);   
        }
        /*
        if (key === 'up') {
            app.setContent('roomSelect', { });
        } else if (key === 'right') {
            app.msg('joinRoom', { resync: true });
        }*/
    }
});