var ui = require('ui'),
    _ = require('common/util'),
    Board = require('./lib/board').Board,
    Particles = require('./lib/particles').Particles,
    ImageView = ui.ImageView,
    TextView = ui.TextView,
    ListView = require('../lib/ListView').ListView;
    
_.extend(exports, {
    init: function(rooms) {
        var self = this;
        self.clear();
        
        self.add('info', new TextView({
            label: 'Please wait, fetching available room...',
            style: {
                width: 'fill-parent',
                height: 'fill-parent',
                align: 'center',
                border: '30 0',
                'font-weight': 'bold'
            }
        }));
        self.isLoaded = false;
        
        app.msg('fetchRooms', { });
    },
    
    showRooms: function(rooms) {
        var self = this;
        self.clear();
        self.isLoaded = true;
        self.isSelected = false;
        
        var list = [];
        var reuse;
        var counter = 0;
        
        // Define create new room button
        reuse = new TextView({
            label: 'Create new room',
            style: {
                width: 'fill-parent',
                border: '4 10'
            }
        });
        reuse.on('focus', function() {
            this.style(app.mystyle.listFocus());
        });
        reuse.on('blur', function() {
            this.style(app.mystyle.listBlur('#ab7c52'));
        });
        reuse.callback = function() {
            self.startGame(this, app.chess.gameTypes.multiplayer, true, 0);
        };
        list.push(reuse);
        
        // Iterate rooms
        rooms.forEach(function(room) {
            var stripeColor = (counter++ % 2 !== 0 ? '#ab7c52' : '#8e6744');
            
            reuse = new TextView({
                label: room.name,
                style: {
                    width: 'fill-parent',
                    border: '4 10'
                }
            });
            reuse.on('focus', function() {
                this.style(app.mystyle.listFocus());
            });
            reuse.on('blur', function() {
                this.style(app.mystyle.listBlur(stripeColor));
            });
            reuse.callback = function() {
                self.startGame(this, app.chess.gameTypes.multiplayer, false, 1, room.id);
            };
            list.push(reuse);
        });
        
        var listView = new ListView(list, 'Available Room');
        self.add('roomsList', listView);
        
        if (self.cachedIndex !== undefined && self.cachedIndex <= list.length) {
            self.get('roomsList').focusItem(self.cachedIndex);
        }

        self.isAutoload = true;
        clearTimeout(self.roomLoadTimer);
        if (self.isAutoload) {
            self.roomLoadTimer = setTimeout(function() {
                self.cachedIndex = self.get('roomsList').index;
                
                app.msg('fetchRooms', { });
            }, 1000 * 3);
        }
        // TODO: this interval should be 10 s
    },
    
    ':load': function() {
        var self = this;
        
        app.on('message', function(action, data) {          
            if (action === 'startGame' || action === 'continueGame') {
                self.isAutoload = false;
                clearTimeout(self.roomLoadTimer);
            }
            
            if (action === 'startGame') {
                console.log('Game starting...');
                app.setContent('main', { gameType: app.chess.gameTypes.multiplayer, playerSide: data.side });
            } else if (action === 'continueGame') {                
                app.setContent('main', { 
                    gameType: app.chess.gameTypes.multiplayer, 
                    playerSide: data.side,
                    resync: true,
                    map: data.resync.map,
                    turn: data.resync.turn });
            } else if (action === 'fetchRooms') {
                self.showRooms(data.rooms);
            }
            
            if (data.roomId) {
                app.chess.storage.set('roomId', { roomId: data.roomId });
            }
        });
    },
    
    ':keypress': function(key) {
        var self = this;
        
        if (self.isLoaded) {
            if (!self.isSelected && (key === 'up' || key === 'down'
                   || key === '84' || key === '116'
                   || key === '66' || key === '98' 
                   || key === 'fire')) {
                       
               if (key === 'fire') {
                   self.isSelected = true;
               }
                       
               this.get('roomsList').emit('keypress', key);   
            }
        }
    },
    
    ':active': function() {
        if (app.isBackFromGame) {
            app.isBackFromGame = false;
            app.back();
        } else {
            this.init();  
            this.isSelected = false;    
        }
        
        console.log('ACTIVE');
    },
    
    ':back': function() {
        console.log('BACK DONK');
        this.isAutoload = false;
        clearTimeout(self.roomLoadTimer);
    },
    
    ':inactive': function() {
        console.log('INACTIVE');
        this.isAutoload = false;
        clearTimeout(self.roomLoadTimer);
    },
    
    startGame: function(listItem, type, isHost, side, roomId) {   
        var self = this;
        
        listItem.label(listItem.label() + ' (wait)');
        self.isSelected = true;
        
        if (type === app.chess.gameTypes.multiplayer) {
            if (isHost) {
                app.msg('createRoom', {  });
            } else {
                app.msg('joinRoom', { roomId: roomId });
            }
        } else {
            app.setContent('main', { gameType: type });
        }
    }
});