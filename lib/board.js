var _ = require('common/util');
var app = this;

var kClear = -1;
var row = function(index) {
    return Math.floor(index / 8);  
};
var col = function(index) {
    return index % 8;
};

function Board() {
	this.map = [];
	this.trash = [];
	this.modified = [];
    this.opponents = [];

	this.score = 0;
	this.multiplier = 1;
	
	// modify the order
	/*
	this.nodeType = {
	    pawn: 0,
	    rook: 1,
	    bishop: 2,
	    knight: 3,
	    queen: 4,
	    king: 5
	};
	*/
	this.nodeType = {
        pawn: 1,
        rook: 5,
        bishop: 2,
        knight: 0,
        queen: 4,
        king: 3
    };
	
	this.playerType = {
	    white: 0,
	    black: 1
	};
	
	// this.init();
	
	// test resync
	// this.resync(storedPos);
}

Board.prototype = {
	at: function(x, y) {
		if (y === undefined) {
			return this.map[x];
		}
		return this.map[y * 8 + x];
	},
	
	// (y * 8) + x
	xy: function(index) {
		return {
			x: Math.floor(index % 8),
			y: Math.floor(index / 8)
		};
	},

    resync: function(remoteMap, storedSide) {
        this.map = [];
        var map = this.map;
        
        this.opponents = [];
        for (n = 0; n < 64; n++) {
            map.push(remoteMap[n]);
            // Add the opponent to opponents array
            if (remoteMap[n].type === nodeType.king) {
                if (remoteMap[n].player === storedSide) {
                    this.king = remoteMap[n];
                } else {
                    this.opponentKing = remoteMap[n];
                }
            }
            if (remoteMap[n].player !== storedSide) {
                this.opponents.push(remoteMap[n]);
            }
            
            this.modified.push(n);
        }
    },

    init: function(storedSide) {
        nodeType = this.nodeType;
        playerType = this.playerType;
        
        this.gemIds = 0;
        this.map = [];
        
        var player;
        var type;
        // Init the white
        for (var n = 0; n < 64; n++) {       
            if ((n >= 0 && n < 16) || (n >= 48 && n < 64)) {
                modded = n % 8;
                if ((n >= 8 && n < 16) || (n >= 48 && n < 56)) {
                    type = nodeType.pawn;
                } else if (modded === 0 || modded === 7) {
                    type = nodeType.rook;
                } else if (modded === 1 || modded === 6) {
                    type = nodeType.bishop;
                } else if (modded === 2 || modded === 5) {
                    type = nodeType.knight;
                } else if (modded === 3) {
                    type = nodeType.queen;
                } else if (modded === 4) {
                    type = nodeType.king;
                }
                
                player = playerType.black;
                if (n >= 48 && n < 64) {
                    player = playerType.white;
                }
                
                this.map.push({
                    id: n,
                    position: n,
                    player: player,
                    type: type,
                    twoBlocks: false,
                    moveCount: 0
                });
                if (this.map[n].type === nodeType.king) {
                    if (this.map[n].player === storedSide) {
                        this.king = this.map[n];
                    } else {
                        this.opponentKing = this.map[n];
                    }
                }
                if (this.map[n].player !== storedSide) {
                    this.opponents.push(this.map[n]);
                }
            } else {
                this.map.push(null);
            }
            
            this.modified.push(n);
        }
    },

    swap: function(src, dst) {
        selectedNode = this.map[src];
        overlappedNode = this.map[dst];
        
        this.map[src] = null;
        this.map[dst] = selectedNode;
        selectedNode.position = dst;
        
        return overlappedNode;
    },

    restore: function(dst, src, tempNode) {
        selectedNode = this.map[dst];
        overlappedNode = tempNode;
        
        this.map[src] = selectedNode;
        this.map[dst] = overlappedNode;
        selectedNode.position = src;
    },

    removeOpponent: function(id) {
        for (var i = 0; i < this.opponents.length; i++) {
            if (this.opponents[i].id === id) {
                this.opponents.splice(i, 1);
            }
        }
    },

	isValidMove: function(node, src, dst, isOpponentVerify) {
	    var side = node.player;
	    var type = node.type;
	    var rV = false;
	    
	    if (src === dst) {
	        return true;
	    }
	    
	    // console.log('Opponents count: ' + this.opponents.length);
	    if (type === this.nodeType.pawn) {
	        rV = this.pawnValidMove(side, src, dst, isOpponentVerify);
	    } else if (type === this.nodeType.rook) {
	        rV = this.rookValidMove(side, src, dst);
	    } else if (type === this.nodeType.bishop) {
	        rV = this.bishopValidMove(side, src, dst);
        } else if (type === this.nodeType.knight) {
            rV = this.knightValidMove(side, src, dst);
        } else if (type === this.nodeType.queen) {
            rV = this.queenValidMove(side, src, dst);
        } else if (type === this.nodeType.king) {
            rV = this.kingValidMove(side, src, dst);
        }
	    
	    // The pawn can possibly move, but need to check the king 'checked' condition
	    // disable this, make the chess game 'obvious' by killing the king
	    /*
	    if (rV && !isOpponentVerify) {
	        var tempNode = this.swap(src, dst);
	        rV = rV & !this.isOwnKingChecked(node);
	        
	        this.restore(dst, src, tempNode);
	    }
	    */
	    
	    return rV;
	},
	// TODO: ENPASSANT ERROR!, opponent gak dapet pesan!
	isOwnKingChecked: function(node) {
	    var self = this;
	    var rV = false;
	    var kingPosition = this.king.position;
	    this.isKingChecked = false;
	    
	    console.log('King pos: ' + kingPosition);
	    var item;
	    for (var i = 0; i < this.opponents.length; i++) {
	        item = this.opponents[i];
	        // console.log('Opponent pos: ' + item.position + ' type: ' + item.type);
	        if (!item.isCaptured && self.isValidMove(item, item.position, kingPosition, true)) {
                rV = true;
                console.log('KING CHECKED by ' + item.type + ' from pos ' + item.position);
                break;
            }
	    }
	    
	    this.isKingChecked = rV;
	    return rV;
	},
	
	isPawnReplace: function(index) {
	    node = this.map[index];
	    rV = false;
	    
	    if (node.type === this.nodeType.pawn) {
	        if (node.player === this.playerType.white && index >= 0 && index < 8) {
	            rV = true;
	        } else if (node.player === this.playerType.black && index >= 56 && index < 64) {
	            rV = true;
	        }
	    }
	    
	    return rV;
	},
	
	isFreeWay: function(side, src, iterator, boundary, isPawn) {
	    var rV = true;
	    
	    var counter = src + iterator;
	    while (true) {
	        if (iterator < 0 && counter < boundary) {
	            break;
	        } else if (iterator > 0 && counter > boundary) {
	            break;
	        }
	        
            // TODO: check player side, friend / foe
            if (counter === boundary && this.map[counter] !== null && this.map[counter].player !== side && !isPawn) {
                this.map[counter].isCaptured = true;
                break;
            } else if (this.map[counter] !== null) {
                rV = false;
                break;
            }
            
            counter += iterator;
        }
        
        
        return rV;
	},
	
	// Node logic definition
	// pion
	pawnValidMove: function(side, src, dst, isOpponentVerify) {
	    var multiplier = (side === 0 ? -1 : 1);
	    var rV = false;
	    var remaining = Math.abs(dst - src);
	    var iterator = 0;
        var boundary = 0;
	    
	    // console.log('Pawn Moving');
	    
	    if ((side === 0 && dst < src) || (side === 1 && dst > src)) {
	        if (!isOpponentVerify && remaining === 16 && this.map[src].moveCount === 0) {
	            // Pawn moves 2 blocks in its initial move
	            // Two blocks also used to determine, whether it is enpassant / not
                this.map[src].twoBlocks = false;
                var currentNode = this.map[src];
                var oppoLeft = null;
                var oppoRight = null;
                if (col(dst) > 1) {
                    oppoLeft = this.map[dst - 1];
                }
                if (col(dst) < 7) {
                    oppoRight = this.map[dst + 1];
                }
                
                console.log('OPPO LEFT:' + oppoLeft);
                console.log('OPPO RIGHT:' + oppoRight);
                if ((oppoLeft !== null && oppoLeft.player !== currentNode.player && oppoLeft.type === this.nodeType.pawn) || 
                    (oppoRight !== null && oppoRight.player !== currentNode.player && oppoRight.type === this.nodeType.pawn)) {
                        
                    this.map[src].twoBlocks = true;
                }
                
                iterator = 8 * multiplier;
                boundary = src + (iterator * 2);
                
                rV = this.isFreeWay(side, src, iterator, boundary, true);
            } else if (!isOpponentVerify && remaining === 8) {
                this.map[src].twoBlocks = false;
                iterator = 8 * multiplier;
                boundary = src + iterator;
                
                rV = this.isFreeWay(side, src, iterator, boundary, true);
            } else if (remaining === 9 || remaining === 7) { 
                // This is where the pawn moves diagonally
                this.map[src].twoBlocks = false;
                
                var enPassantProj = (remaining === 7 ? 1 : -1);
                if (side === 1) {
                    enPassantProj = enPassantProj * -1;
                }
                var enPassant = this.map[src + enPassantProj];
                if (enPassant !== null && enPassant.twoBlocks && enPassant.moveCount === 1) {
                    // This is where the pawn is in en passant state
                    
                    rV = true;
                    this.enPassant = true;
                    this.enPassantIndex = src + enPassantProj;
                    this.enPassantNullIndex = src;
                } else {
                    // CHECK KING PRESENCE!
                    var overlappedNode = this.map[dst];
                    if (overlappedNode !== null && overlappedNode.player !== side) {
                        overlappedNode.isCaptured = true;
                        rV = true;
                    }
                }
            } else {
                rV = false;
            }
	    } else {
	        rV = false;
	    }
	    
	    return rV;
	},
	
	// benteng
	// TODO: kurang castling
	rookValidMove: function(side, src, dst) {
	    var remaining = dst - src;
	    var rV = false;
	    var iterator = 0;
	    var multiplier = 1;
	    var boundary = 0;
	    
	    if (dst < src) {
	        multiplier = -1;
	    }
	    
	    if ((row(src) === row(dst))) {
	        // Move by column
	        iterator = multiplier;
	        boundary = Math.min(dst, ((row(src) + 1) * 8) - 1);
	        
	        if (multiplier < 0) {
	            boundary = Math.max(dst, (row(src) * 8));
	        }
	    } else if ((col(src) === col(dst))) {
	        // Move by row
	        iterator = 8 * multiplier;
	        boundary = Math.min(dst, 63);
            
            if (multiplier < 0) {
                boundary = Math.max(dst, 0);
            }
	    } else {
	        return false;
	    }
	    
	    rV = this.isFreeWay(side, src, iterator, boundary);
	    
        return rV;
    },
    
    // concom
    bishopValidMove: function(side, src, dst) {
        var remaining = dst - src;
        var rV = false;
        var iterator = 0;
        var multiplier = 1;
        var boundary = 0;
        
        if (dst < src) {
            multiplier = -1;
        }
        
        var x1 = col(src);
        var y1 = row(src);
        var x2 = col(dst);
        var y2 = row(dst);
        var gradient = Math.abs((y2 - y1) / (x2 - x1));
        if (gradient === 1 && (remaining % 7 === 0)) {
            // Move diagonal upper right to bottom left
            iterator = 7 * multiplier;
            boundary = Math.min(dst, 63);
            
            if (multiplier < 0) {
                boundary = Math.max(dst, 0);
            }
        } else if (gradient === 1 && (remaining % 9 === 0)) {
            // Move diagonal upper left to bottom right
            iterator = 9 * multiplier;
            boundary = Math.min(dst, 63);
            
            if (multiplier < 0) {
                boundary = Math.max(dst, 0);
            }
        } else {
            return false;
        }
        
        rV = this.isFreeWay(side, src, iterator, boundary);
        
        return rV;
    },
    
    // kuda
    knightValidMove: function(side, src, dst) {
        var remaining = Math.abs(dst - src);
        var rV = false;
        
        if (remaining === 10 || remaining === 17 || 
            remaining === 15 || remaining === 6) {
                
            var overlappedNode = this.map[dst];
            if (overlappedNode === null || overlappedNode.player !== side) {
                overlappedNode.isCaptured = true;
                rV = true;
            }            
        }
        
        return rV;
    },
    
    // ster
    queenValidMove: function(side, src, dst) {
        return (this.rookValidMove(side, src, dst) || this.bishopValidMove(side, src, dst));
    },
    
    // raja
    kingValidMove: function(side, src, dst) {
        var rV = false;
        var deltaRow = Math.abs(row(src) - row(dst));
        var deltaCol = Math.abs(col(src) - col(dst));
        
        if (deltaRow < 2 && deltaCol < 2) {
            
            // Check casting move
            if (this.map[src].moveCount === 0 && Math.abs(src - dst) === 1) {
                if (dst === (src - 1)) {
                    iterator = -1;
                    boundary = src - 3;
                    rookPos = src - 4;
                } else {
                    iterator = 1;
                    boundary = src + 2;
                    rookPos = src + 3;
                }
                
                rook = this.map[rookPos];
                isFree = this.isFreeWay(side, src, iterator, boundary);
                if (isFree && rook !== null && rook.type === this.nodeType.rook && rook.moveCount === 0) {
                    this.castling = true;
                    this.rookPos = rookPos;
                    this.kingPos = src + iterator; 
                    this.nextKingPos = src + (iterator * 2);
                    this.nextRookPos = this.kingPos;
                    this.castlingCounter = 3;
                }
            }
            
            var overlappedNode = this.map[dst];
            if (overlappedNode === null || overlappedNode.player !== side) {
                rV = true;
                overlappedNode.isCaptured = true;
            }
        }
        
        return rV;
    }
};

exports.Board = Board;
