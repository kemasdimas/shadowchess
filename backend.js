var http = require('blaast/simple-http'),
    _ = require('underscore'),
    sys = require('sys'),
    jsdom = require('jsdom'),
    QS = require('querystring'),
    Scaling = require('blaast/scaling').Scaling,
    ReqLog  = require('blaast/mark').RequestLogger;
    
var storage = require('blaast/simple-data');

var scaling     = new Scaling(app.config);
var rlog        = new ReqLog(app.log);
var ShadowChess = { };

ShadowChess.players = { };
ShadowChess.rooms   = { };
ShadowChess.timeout = 1000 * 60 * 10; // Timeout in 10 minutes
ShadowChess.timeoutTolerance = 0; // Timeout tolerance is 1 minute

function ShadowChessRoom(host, name, id) {
    var self = this;
    self.players = [ ];
    self.playersId = { };
    self.name = name;
    self.id = id;
    self.board = [ ];
    self.turn = -1;
    self.chatMessages = [];
    
    self.players.push(host);
    self.playersId[host.client.id] = 0;
}
ShadowChessRoom.prototype = {
    
    createRoom: function() {
        ShadowChess.rooms[this.id] = this;
    },
    
    joinRoom: function(guest) {
        if (this.playersId[guest.client.id] !== undefined) {
            return true;
        } else if (this.players.length < 2) {
            this.playersId[guest.client.id] = 1;
            this.players.push(guest);
            
            return true;
        } else {
            return false;
        }
    },
    
    leaveRoom: function(side) {
        var self = this;
        
        // Properly modify side if it is only one player in room
        if (side === 1 && self.players.length < 2) {
            side = 0;
        }
        var leavingPlayer = self.players[side];
        delete ShadowChess.players[leavingPlayer.client.id];
        
        // properly delete a player
        if (side === 1) {
            self.players.pop();   
        } else {
            self.players.shift();
        }
        delete self.playersId[leavingPlayer.client.id];
        
        // There will be only one player, send him a congratulation
        if (self.players.length > 0) {
            log.info('Congratulate player ' + self.players[0].side);
            var cause = '';
            if (self.opponentTimeout) {
                cause = 'Opponent idle. ';
            } else if (self.opponentSurrender) {
                cause = 'Opponent surrender. ';
            } else {
                cause = 'Opponent king captured. ';
            }
            
            self.players[0].playerWin(cause);
            self.players[0].leaveRoom();
        } else {
            self.destroyRoom();
        }
    },
    
    destroyRoom: function() {
        clearInterval(this.timerInterval);
        delete ShadowChess.rooms[this.id];
        
        console.dir(ShadowChess);
    },
    
    playerReady: function(side, isResync) {
        if (this.players[side]) {
            this.players[side].isReady = true;
        }
        
        if (this.countReadyPlayer() >= 2) {
            
            // Broadcast the players data
            var lightName = this.players[0].client.user.id;
            var darkName = this.players[1].client.user.id;
            this.players.forEach(function(player) {
                player.setPlayers(lightName, darkName);
            });
            
            if (isResync) {
                this.players[this.turn].setTurn(this.getRemainingTimeout());
            } else {
                this.turn = 0;
                this.resetTimeout();
                this.players[0].setTurn(this.getRemainingTimeout());
            }
        }
    },
    
    countReadyPlayer: function() {
        var count = 0;
        
        this.players.forEach(function(player) {
            if (player.isReady) {
                count++;
            }    
        });
        
        return count;
    },
    
    playerSurrender: function(side) {        
        this.opponentSurrender = true;
        clearInterval(this.timerInterval);
                
        this.players[side].playerSurrender();
        this.players[side].leaveRoom();
    },
    
    kingCaptured: function(capturer, captured) {
        clearInterval(this.timerInterval);
                
        this.players[captured].playerKingCaptured();
        this.players[captured].leaveRoom();
    },
    
    sendChatMessage: function(sender, text) {
        // Save the messages
        this.chatMessages.push({
            sender: sender,
            text: text
        });
        
        var destination = (sender === 0 ? 1 : 0);
        
        this.players.forEach(function(player) {
            player.sendChatMessage(sender, text); 
        });
    },
    
    syncMessage: function(requester) {
        this.players[requester].messageSynched(this.chatMessages);
    },
    
    resetTimeout: function() {
        var self = this;
        time = new Date().getTime();
        
        clearInterval(this.timerInterval);
        // this.timeoutBase = time;
        this.timeout = time + ShadowChess.timeout + ShadowChess.timeoutTolerance;
        
        // Interval executed each 10 seconds to provide tolerance
        this.timerInterval = setInterval(function() {
            if (self.isPlayerTimeout()) {
                self.opponentTimeout = true;
                clearInterval(self.timerInterval);
                
                self.players[self.turn].timeoutKick();
                self.players[self.turn].leaveRoom();
            }
            
            log.info('Timer is Ticking... -> ' + self.getRemainingTimeout());
        }, 10 * 1000);
    },
    
    isPlayerTimeout: function() {
        return (this.getRemainingTimeout() <= 0);
    },
    
    /**
     * Get remaining timeout in Second
     */
    getRemainingTimeout: function() {
        time = new Date().getTime();
        
        return Math.ceil((this.timeout - ShadowChess.timeoutTolerance - time) / 1000);
    },
    
    switchTurn: function(playerSide) {
        console.log('PlayerSide: ' + playerSide + ' TurnNow: ' + this.turn);
        if (playerSide === this.turn) {
            if (this.turn === 0) {
                this.turn = 1;
            } else {
                this.turn = 0;
            }
            
            this.resetTimeout();
            this.players[this.turn].setTurn(this.getRemainingTimeout());
        }
    },
    
    moveTo: function(side, src, dst, twoBlocks, moveCount) {
        var oppoSide = (side === 0 ? 1 : 0);
        
        if (this.map[src] !== null) {
            this.map[src].twoBlocks = twoBlocks;
            this.map[src].moveCount = moveCount;
        }
        
        console.dir(this.map[src]);
        this.swap(src, dst);
        this.players[oppoSide].moveTo(src, dst, twoBlocks, moveCount);
    },
    
    swap: function(src, dst) {
        tempNode = this.map[src];
        this.map[src] = null;
        this.map[dst] = tempNode;
        
        if (tempNode !== null) {
            tempNode.position = dst;
        }
    },
    
    replacePawn: function(index, repType) {
        tempNode = this.map[index];
        tempNode.type = repType;
        
        this.players.forEach(function(player) {
            player.replacePawn(index, repType);
        });
    },
    
    initBoard: function(map) {
        if (!this.initialized) {
            this.map = map;
            this.initialized = true;
        }
    },
    
    resync: function(side) {
        this.players[side].resyncSuccess(this.map, this.turn);
    }
};

function ShadowChessPlayer(client) {
    var self = this;
    self.action = null;
    self.client = client;
    self.roomId = null;
    self.isHost = false;
    self.side = 1;
    self.score = 0;
}
ShadowChessPlayer.prototype = {
    /**
     * Create new room
     */
    createRoom: function() {
        var self = this;
        
        var timeStamp = new Date().getTime().toString();
        var room = new ShadowChessRoom(self, self.client.user.id, self.client.id + '-' + timeStamp);
        room.createRoom();
        
        self.isHost = true;
        self.side = 0;
        self.roomId = room.id;
        
        // Send created msg to client
        self.client.msg('startGame', { isHost: self.isHost, side: self.side, roomId: self.roomId });
    },
    
    /**
     * Used by the guest to join a room
     */
    joinRoom: function(args) {
        var self = this;
        var roomId = args.roomId;
        var resync = args.resync;
        if (resync) {
            roomId = ShadowChess.players[self.client.id].roomId;
        }
        
        var room = ShadowChess.rooms[roomId];
        if (room !== undefined) {
            if (room.joinRoom(self)) {
                self.roomId = roomId;
                
                if (resync) {
                    timeout = room.getRemainingTimeout();
                    obj = { isHost: self.isHost, side: self.side, resync: { map: room.map, turn: room.turn, timeout: timeout, roomId: self.roomId } };
                    console.dir(obj);
                    
                    self.client.msg('continueGame', obj);
                } else {
                    // Send joined msg to client
                    self.client.msg('startGame', { isHost: self.isHost, side: self.side, roomId: self.roomId });
                }
                
                return;
            }
        }
        
        // Failed to join room, delete player
        self.client.msg('noRoom', { });
        delete ShadowChess.players[self.client.id];
    },
    
    /**
     * Player leaving room
     */
    leaveRoom: function() {
        var room = ShadowChess.rooms[this.roomId];
        if (room !== undefined) {
            room.leaveRoom(this.side);
        }
        
        log.info('Player ' + this.side + ' is leaving.');
    },
    
    setPlayers: function(light, dark) {
        var self = this;
        
        self.client.msg('setPlayers', { light: light, dark: dark });
    },
    
    initBoard: function(args) {
        var room = ShadowChess.rooms[this.roomId];
        if (room !== undefined) {
            room.initBoard(args.map);
        }
    },
    
    resync: function() {
        var room = ShadowChess.rooms[this.roomId];
        if (room !== undefined) {
            room.resync(this.side);
        }
    },
    
    resyncSuccess: function(map, turn) {
        // console.dir({ positions: positions, turn: turn });
        
        this.client.msg('resync', { map: map, turn: turn });
    },
    
    playerReady: function(args) {
        var room = ShadowChess.rooms[this.roomId];
        if (room !== undefined) {
            room.playerReady(args.side, args.resync);
        }
    },
    
    setTurn: function(remaining) {
        this.client.msg('setTurn', { side: this.side, remaining: remaining });
    },
    
    switchTurn: function(args) {
        var room = ShadowChess.rooms[this.roomId];
        if (room !== undefined) {
            room.switchTurn(args.side);
        }
    },
    
    handleMoveTo: function(args) {
        var src = args.selectedIndex;
        var dst = args.destinatedIndex;
        var twoBlocks = args.twoBlocks;
        var moveCount = args.moveCount;
        
        var room = ShadowChess.rooms[this.roomId];
        if (room !== undefined) {
            room.moveTo(this.side, src, dst, twoBlocks, moveCount);
        }
    },
    
    moveTo: function(src, dst, twoBlocks, moveCount) {
        log.info('Move Count: ' + moveCount);
        this.client.msg('moveTo', { selectedIndex: src, destinatedIndex: dst, twoBlocks: twoBlocks });
    },
    
    handleReplacePawn: function(args) {
        var index = args.selectedIndex;
        var repType = args.replacementType;
        
        var room = ShadowChess.rooms[this.roomId];
        if (room !== undefined) {
            room.replacePawn(index, repType);
        }
    },
    
    handlePlayerSurrender: function() {
        var room = ShadowChess.rooms[this.roomId];
        if (room !== undefined) {
            room.playerSurrender(this.side);
        }
    },
    
    handleKingCaptured: function(args) {
        var capturer = args.capturer;
        var captured = args.captured;
        
        var room = ShadowChess.rooms[this.roomId];
        if (room !== undefined) {
            room.kingCaptured(capturer, captured);
        }
    },
    
    handleChatMessage: function(args) {
        var side = args.side;
        var text = args.text;
        
        var room = ShadowChess.rooms[this.roomId];
        if (room !== undefined) {
            room.sendChatMessage(side, text);
        }
        
        this.client.msg('chatAcknowledge', { });
    },
    
    sendChatMessage: function(sender, text) {
        this.client.msg('chatMessage', { sender: sender, text: text });
    },
    
    messageSynched: function(messages) {
        console.dir(messages);
        this.client.msg('messageSynched', { messages: messages });
    },
    
    handleSyncMessage: function() {
        var self = this;
        
        var room = ShadowChess.rooms[this.roomId];
        if (room !== undefined) {
            room.syncMessage(self.side);
        }
    },
    
    playerSurrender: function() {
        this.opponentSurrender = true;
        this.client.msg('playerLose', { cause: ' You\'ve left the game.',  score: this.score });
    },
    
    playerKingCaptured: function() {
        this.client.msg('playerLose', { cause: ' Your king captured.',  score: this.score });
    },
    
    replacePawn: function(index, repType) {
        this.client.msg('replacePawn', { selectedIndex: index, replacementType: repType });
    },
    
    playerWin: function(cause) {
        this.client.msg('playerWin', { score: this.score, cause: cause });
    },
    
    timeoutKick: function() {
        this.client.msg('timeoutKick', { score: this.score, timeoutMax: Math.ceil(ShadowChess.timeout / 1000) });
    }
};

function fetchRooms(client) {
    var rooms = [];
    
    
    Object.keys(ShadowChess.rooms).forEach(function(key) {
        var room = ShadowChess.rooms[key];
        
        if (room.countReadyPlayer() < 2) {
            rooms.push({
                name: room.name,
                id: room.id
            });    
        }   
    });
    
    // log.info('ROOM FETCHED');
    client.msg('fetchRooms', { rooms: rooms });
}

app.message(function(client, action, args) {
    var self = this;
    app.debug(client.header() + ' action="' + action + '"');
    self.client = client;
    
    if (action === 'fetchRooms') {
        fetchRooms(client);
    } else if (action.length > 0 && action.charAt(0) !== '_' && ShadowChessPlayer.prototype.hasOwnProperty(action)) {
        if (ShadowChess.players[client.id] === undefined) {
            ShadowChess.players[client.id] = new ShadowChessPlayer(client);
        }
        
        var user = ShadowChess.players[client.id];
        user.action = action;
        user[action].call(user, args);
    } else {
        app.debug(client.header() + ' unknown-action="' + action + '"');
    }
    
    // console.dir(ShadowChess);
});
